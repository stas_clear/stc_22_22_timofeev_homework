import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();
    List<Product> findById(Integer id);

    List<Product> findAllByTitleLike(String productName);

    void updateProduct(Product product);

}
