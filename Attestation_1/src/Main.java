import java.util.ArrayList;
public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("productList.txt");

        System.out.println(productsRepository.findById(1));
        System.out.println(productsRepository.findAllByTitleLike("мОЛо"));
        System.out.println(productsRepository.findAllByTitleLike("коко"));


        Product upProduct = (Product) productsRepository.findById(1);
        upProduct.setPrice(444.1);
        upProduct.setInStock(222.0);
        productsRepository.updateProduct(upProduct);

        System.out.println(upProduct);
    }
}