import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public static final Function<String, Product> stringToProductMapper = currentProducts -> {
        String[] parts = currentProducts.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String productName = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Double inStock = Double.parseDouble(parts[3]);
        return new Product(id, productName, price, inStock);
    };

    public static final Function<Product, String> productToStringMapper = product ->
                product.getId() + "|" +
                product.getProductName() + "|" +
                product.getPrice() + "|" +
                product.getInStock();

    @Override
    public List<Product> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String productName) {
       try {
           return new BufferedReader(new FileReader(fileName))
                   .lines()
                   .map(stringToProductMapper)
                   .filter(product -> product.getProductName().toLowerCase().contains(productName.toLowerCase()))
                   .collect(Collectors.toList());
       } catch (IOException e) {
           throw new IllegalArgumentException(e);
       }
    }

    @Override
    public void updateProduct(Product product) {

        try(FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            List<Product> previousProductList = bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());

            for (Product productLast : previousProductList) {
                if (productLast.getId() == product.getId()) {
                    previousProductList.set(previousProductList.indexOf(productLast), product);
                }
            }

            try(FileWriter fileWriter = new FileWriter(fileName, false);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
                for (Product productNew : previousProductList) {
                    String newProduct = productToStringMapper.apply(productNew);
                    bufferedWriter.write(newProduct);
                    bufferedWriter.newLine();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}