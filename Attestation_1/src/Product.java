import java.util.Objects;

public class Product {
    private Integer id;
    private String productName;
    private Double price;
    private Double inStock;

    public Product(Integer id, String product, Double price, Double inStock) {
        this.id = id;
        this.productName = product;
        this.price = price;
        this.inStock = inStock;
    }

    public Product(String product, Double price, Double inStock) {
        this.productName = product;
        this.price = price;
        this.inStock = inStock;
    }

    public Integer getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public Double getPrice() {
        return price;
    }

    public Double getInStock() {
        return inStock;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setInStock(Double inStock) {
        this.inStock = inStock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", product='" + productName + '\'' +
                ", price=" + price +
                ", inStock=" + inStock +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) && Objects.equals(productName, product.productName) && Objects.equals(price, product.price) && Objects.equals(inStock, product.inStock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productName, price, inStock);
    }
}