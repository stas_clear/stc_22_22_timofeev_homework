package ru.inno.clearshop.services;

import ru.inno.clearshop.dto.LessonForm;
import ru.inno.clearshop.models.Lesson;
import java.util.List;

public interface LessonsService {
    List<Lesson> getAllLesson();

    void addLesson(LessonForm lesson);

    Lesson getLesson(Long lessonId);

    void addLessonToCourse(Long courseId, Long lessonId);

    void deleteLesson(Long lessonId);
}
