package ru.inno.clearshop.services;

import ru.inno.clearshop.dto.UserForm;

public interface SignUpService {
    void signUp(UserForm userForm);
}
