package ru.inno.clearshop.services;

import ru.inno.clearshop.dto.UserForm;
import ru.inno.clearshop.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}


