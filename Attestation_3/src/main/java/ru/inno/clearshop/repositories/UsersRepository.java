package ru.inno.clearshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.clearshop.models.Course;
import ru.inno.clearshop.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByCoursesNotContainsAndState(Course course, User.State state);

    List<User> findAllByCoursesContains(Course course);

    Optional<User> findByEmail(String email);
}