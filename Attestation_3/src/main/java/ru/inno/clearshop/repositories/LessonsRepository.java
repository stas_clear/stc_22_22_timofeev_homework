package ru.inno.clearshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.clearshop.models.Lesson;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByStateNot(Lesson.State state);
}
