package ru.inno.clearshop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseForm {
    private String title;
    private String description;
    @DateTimeFormat (pattern = "yyyy-MM-dd")
    private Date start;
    @DateTimeFormat (pattern = "yyyy-MM-dd")
    private Date finish;
}
