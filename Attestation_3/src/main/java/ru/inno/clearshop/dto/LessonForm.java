package ru.inno.clearshop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonForm {
    private String title;
    private String description;

    private String start;
    private String finish;

}






