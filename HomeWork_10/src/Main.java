import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        String[] string = "Hello Hello bye Hello bye Inno".split(" ");
        HashMap<String, Integer> map = new HashMap<>();
        String s = null;
        int p = 0;
        for (String word : string) {
            if (map.containsKey(word)) {
                map.put(word, map.get(word) + 1);
                if (p < map.get(word)) {
                    p = map.get(word);
                    s = word;
                }
            } else {
                map.put(word, 1);
            }
        }
        System.out.println(s + " " + map.get(s));
    }
}