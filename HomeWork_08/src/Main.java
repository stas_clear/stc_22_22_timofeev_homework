public class Main {
    public static void main(String[] args) {

        ArrayTask sumArrayFromTo = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            System.out.println("\r");
            return sum;
        };

        ArrayTask maxNumberInRange = (array, from, to) -> {
            int max = array[from];
            for (int i = from; i < to; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            System.out.println("\rМаксимальное число в диапазоне: " + max);
            int num = max;
            int sum = 0;
            while (num > 0) {
                sum += num % 10;
                num /= 10;
            }
            return sum;
        };

        int[] array = {4, 3, 8, 432, 7, 11, 9, 1, 15, 14};
        ArraysTasksResolver.resolveTask(array, sumArrayFromTo, 3, 6);
        ArraysTasksResolver.resolveTask(array, maxNumberInRange, 3, 6);
    }
}