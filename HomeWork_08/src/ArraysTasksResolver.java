public class ArraysTasksResolver {
    static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println("Диапазон массива с " + from + " по " + to + " элемент." );
        for (int i = from; i <= to; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("Результат: " + task.resolve(array, from, to));
    }
}
