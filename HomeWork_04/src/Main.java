import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // 1. Сумма чисел в интервале
        System.out.print("Начальное число: ");
        int numStart = scanner.nextInt();
        System.out.print("Конечное число: ");
        int numEnd = scanner.nextInt();
        System.out.println("Сумма чисел: " + sumNumbersInRange(numStart, numEnd));

        // 2. Чётные элементы массива
        int[] array = new int[] {78, 89, 44, 73, 16, 6, 11};
        System.out.println("Чётные числа в массиве: ");
        isEven(array);

        // 3. Число в виде массива
        int[] arr = new int[] {4, 8, 7, 1, 9};
        toInt(arr);
    }

    public static void toInt (int[] a) {
        int number = 0;
        for (int j : a) {
            number = number * 10 + j;
        }
        System.out.println("Число из массива: " + number);
    }

    public static void isEven (int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                System.out.println(a[i]);
            }
        }
    }

    public static int sumNumbersInRange(int from, int to) {
        int sum = 0;
        if (from <= to) {
            for (int i = from; i <= to; i++) {
                sum += i;
            }
        } else {
            return -1;
        }
        return sum;
    }

}