package ru.inno.clearshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.clearshop.dto.LessonForm;
import ru.inno.clearshop.models.Course;
import ru.inno.clearshop.models.Lesson;
import ru.inno.clearshop.models.User;
import ru.inno.clearshop.repositories.CoursesRepository;
import ru.inno.clearshop.repositories.LessonsRepository;
import ru.inno.clearshop.services.LessonsService;

import java.time.LocalTime;
import java.util.List;
@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;
    private final CoursesRepository coursesRepository;

    @Override
    public List<Lesson> getAllLesson() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public Lesson getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .title(lesson.getTitle())
                .description(lesson.getDescription())
                .start(LocalTime.parse(lesson.getStart()))
                .finish(LocalTime.parse(lesson.getFinish()))
                .state(Lesson.State.ACTIVE)
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public void addLessonToCourse(Long courseId, Long lessonId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();
        lesson.setCourse(course);
        lessonsRepository.save(lesson);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lessonForDelete = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDelete.setState(Lesson.State.DELETED);

        lessonsRepository.save(lessonForDelete);
    }


}
