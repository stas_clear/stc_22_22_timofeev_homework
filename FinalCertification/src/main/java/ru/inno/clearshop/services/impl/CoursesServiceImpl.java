package ru.inno.clearshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.clearshop.dto.CourseForm;
import ru.inno.clearshop.models.Course;
import ru.inno.clearshop.models.Lesson;
import ru.inno.clearshop.models.User;
import ru.inno.clearshop.repositories.CoursesRepository;
import ru.inno.clearshop.repositories.LessonsRepository;
import ru.inno.clearshop.repositories.UsersRepository;
import ru.inno.clearshop.services.CoursesService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CoursesServiceImpl implements CoursesService {
    private final CoursesRepository coursesRepository;
    private final UsersRepository usersRepository;

    private final LessonsRepository lessonsRepository;
    @Override
    public List<Course> getAllCourse() {
        return coursesRepository.findAll();
    }

    @Override
    public void addCourse(CourseForm course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .description(course.getDescription())
                .start(course.getStart())
                .finish(course.getFinish())
                .build();
        coursesRepository.save(newCourse);
    }

    @Override
    public void addUserToCourse(Long courseId, Long userId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        User user = usersRepository.findById(userId).orElseThrow();

        user.getCourses().add(course);

        usersRepository.save(user);
    }

    @Override
    public Course getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }

    @Override
    public List<User> getNotInCourseUsers(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContainsAndState(course, User.State.CONFIRMED);
    }

    @Override // Ученики в Курсе.
    public List<User> getInCourseUsers(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContains(course);
    }

    @Override // Уроки в Курсе
    public List<Lesson> getInCourseLessons(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByLessonsContains(course);
    }

    @Override
    public void updateCourse(Long courseId, CourseForm updateData) {
        Course courseForUpdate = coursesRepository.findById(courseId).orElseThrow();

        courseForUpdate.setTitle(updateData.getTitle());
        courseForUpdate.setDescription(updateData.getDescription());
        courseForUpdate.setStart(updateData.getStart());
        courseForUpdate.setFinish(updateData.getFinish());

        coursesRepository.save(courseForUpdate);
    }
}