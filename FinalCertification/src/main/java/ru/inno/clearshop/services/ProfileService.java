package ru.inno.clearshop.services;

import ru.inno.clearshop.models.User;
import ru.inno.clearshop.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}
