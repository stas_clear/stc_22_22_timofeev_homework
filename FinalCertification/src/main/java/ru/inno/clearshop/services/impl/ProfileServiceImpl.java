package ru.inno.clearshop.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.clearshop.models.User;
import ru.inno.clearshop.repositories.UsersRepository;
import ru.inno.clearshop.security.details.CustomUserDetails;
import ru.inno.clearshop.services.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }
}
