package ru.inno.clearshop.services;

import ru.inno.clearshop.dto.CourseForm;
import ru.inno.clearshop.dto.UserForm;
import ru.inno.clearshop.models.Course;
import ru.inno.clearshop.models.Lesson;
import ru.inno.clearshop.models.User;

import java.util.List;

public interface CoursesService {
    List<Course> getAllCourse();

    void addCourse(CourseForm course);
    void addUserToCourse(Long courseId, Long userId);

    Course getCourse(Long courseId);

    List<User> getNotInCourseUsers(Long courseId);

    List<User> getInCourseUsers(Long courseId);
    List<Lesson> getInCourseLessons(Long courseId);

    void updateCourse(Long courseId, CourseForm course);
}
