package ru.inno.clearshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.clearshop.models.Course;
import ru.inno.clearshop.models.Lesson;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByStateNot(Lesson.State state);
    List<Lesson> findAllByLessonsContains (Course course);
}
