package ru.inno.clearshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.clearshop.models.Course;

public interface CoursesRepository extends JpaRepository<Course, Long> {
}
