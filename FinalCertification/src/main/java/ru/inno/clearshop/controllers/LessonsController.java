package ru.inno.clearshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.clearshop.dto.LessonForm;
import ru.inno.clearshop.security.details.CustomUserDetails;
import ru.inno.clearshop.services.LessonsService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/lessons")
public class LessonsController {
    private final LessonsService lessonsService;

    @GetMapping
    public String getLessonsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("lessons", lessonsService.getAllLesson());
        return "lessons/lessons_page";
    }

    @GetMapping("/{lesson-id}")
    public String getLessonPage(@PathVariable("lesson-id") Long lessonId, Model model) {
        model.addAttribute("lesson", lessonsService.getLesson(lessonId));
        return "lessons/lesson_page";
    }


    @PostMapping
    public String addLesson(LessonForm lesson) {
        lessonsService.addLesson(lesson);
        return "redirect:/lessons";
    }

    @PostMapping("/{course-id}/lessons")
    public String addLessonToCourse(@PathVariable("course-id") Long courseId,
                                    @RequestParam("lesson-id") Long lessonId) {
        lessonsService.addLessonToCourse(courseId, lessonId);
        return "redirect:/lessons/" + lessonId;
    }

    @GetMapping("/{lesson-id}/delete")
    public String updateLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonsService.deleteLesson(lessonId);
        return "redirect:/lessons/";
    }
}
