package ru.inno.clearshop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.clearshop.dto.CourseForm;
import ru.inno.clearshop.dto.UserForm;
import ru.inno.clearshop.security.details.CustomUserDetails;
import ru.inno.clearshop.services.CoursesService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/courses")
public class CoursesController {

    private final CoursesService coursesService;

    @GetMapping
    public String getCoursesPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("courses", coursesService.getAllCourse());
        return "courses/courses_page";
    }

    @PostMapping
    public String addCourse(CourseForm course) {
        coursesService.addCourse(course);
        return "redirect:/courses";
    }

    @PostMapping("/{course-id}/users")
    public String addUserToCourse(@PathVariable("course-id") Long courseId,
                                  @RequestParam("user-id") Long userId) {
        coursesService.addUserToCourse(courseId, userId);
        return "redirect:/courses/" + courseId;
    }

    @GetMapping("/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseUsers", coursesService.getNotInCourseUsers(courseId));
        model.addAttribute("inCourseUsers", coursesService.getInCourseUsers(courseId));
        return "courses/course_page";
    }

    @PostMapping("/{course-id}/update")
    public String updateCourse(@PathVariable("course-id") Long courseId, CourseForm course) {
        coursesService.updateCourse(courseId, course);
        return "redirect:/courses/" + courseId;
    }
}
