public class Atm {
    private final int MAX_SUM_ISSUED = 50000;   // Максимальная сумма резрешенная к выдаче
    private final int MAX_SUM_ATM = 1000000;    // Максимальный объём денег в банкомате
    int quantityRestMoney;                  // Сумма оставшихся денег
    int quantityOperations = 0;             // Проведенных операций

    // Конструктор
    Atm (int quantityRestMoney, int quantityOperations) {
        this.quantityRestMoney = quantityRestMoney;
        this.quantityOperations = quantityOperations;
    }

    // Внесение денег
   int balance (int receive) {
        if (quantityRestMoney + receive <= MAX_SUM_ATM ) {
            // Внесение разрешено
            quantityRestMoney += receive;
            quantityOperations++;
            System.out.println("Пополнение на " + receive + " руб. На балансе " + quantityRestMoney + " руб. Операция № " + quantityOperations);
        }
        return receive;
    }

    void requestedMoney (int sum) {
        if (sum <= MAX_SUM_ISSUED && sum <= quantityRestMoney) {
            quantityRestMoney -= sum;
            quantityOperations++;
            System.out.println("Выдано " + sum + " руб. Остаток " + quantityRestMoney + " руб. Операция № " + quantityOperations);

        } else if (sum > MAX_SUM_ISSUED) {
            System.err.println("Запрошена слишком большая сумма. Максимально к выдаче " + MAX_SUM_ISSUED + " руб.");
        } else {
            System.err.println("В банкомате закончились деньги.");
        }
    }

}