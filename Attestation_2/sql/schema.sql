--drop table if exists driver;
--drop table if exists auto;
--drop table trip;
--drop table if exists auto;

create table driver
(
    id serial primary key,
    first_name char(20),
    last_name char(20),
    age integer check ( age >= 18 and age <= 100 ),
    phone char (20) not null,
    experience integer check ( experience >= 0 and experience <= 100 ),
    is_license bool,
    license_cat char(10),
    rating double precision check ( rating >= 0 and rating <= 5 ) default 0

);

create table auto (
                      id serial primary key,
                      model char(20) not null,
                      color char(20),
                      number char(20) unique not null,
                      driver_id bigint not null,
                      foreign key (driver_id) references driver (id)
);

create table trip (
                      id serial primary key,
                      driver_id bigint,
                      auto_id bigint,
                      foreign key (driver_id) references driver(id),
                      foreign key (auto_id) references auto(id),
                      date_trip date,
                      duration time
);