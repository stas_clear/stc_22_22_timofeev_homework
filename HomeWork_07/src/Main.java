public class Main {
    public static void main(String[] args) {

        EvenNumbersPrintTask arr1 = new EvenNumbersPrintTask(40,55);
        OddNumbersPrintTask arr2 = new OddNumbersPrintTask(12, 20);
        EvenNumbersPrintTask arr3 = new EvenNumbersPrintTask(17,30);
        OddNumbersPrintTask arr4 = new OddNumbersPrintTask(85, 100);

        Task[] tasks = {arr1, arr2, arr3, arr4};
        completeAllTask(tasks);
    }
    public static void completeAllTask(Task[] tasks){
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }

}

