abstract class AbstractNumbersPrintTask implements Task {
    protected final int from;
    protected final int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public abstract void complete();
}
