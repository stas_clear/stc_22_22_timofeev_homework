package ru.inno;

public interface Collection<T> {
    void add(T element);
    void remove(T element);
    boolean contain(T element);

    int size ();

}
