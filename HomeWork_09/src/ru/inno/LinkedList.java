package ru.inno;

public class LinkedList<T> implements List<T> {

    private Node<T> first;
    private Node<T> last;
    private int count;

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

    @Override
    public void add(T element) {
        // Создаём новый узел со значением
        Node<T> newNode = new Node<>(element);
        if (count == 0) {
            this.first = newNode;
        } else {
            // если в списке уже есть узел, то у последнего делаем следующий новый узел
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {

    }

    @Override
    public boolean contain(T element) {
        Node<T> current = this.first;
        while (current != null) {
            if (current.value.equals(element)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {    }

    @Override
    public T get(int index) {
        if (index >= 0 && index <= count) {
            Node<T> current = this.first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        return null;
    }
}
