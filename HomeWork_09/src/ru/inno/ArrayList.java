package ru.inno;

public class ArrayList<T> implements List<T> {
    private static final int DEFAULT_ARRAY_SIZE = 10;
    private T[] elements;
    private int count;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        if (isFool()) { // если переполнен увеличиваем и перезаписываем в новый массив
            resize();
        }
        // добавляем новый элемент в массив
        elements[count] = element;
        count++;
    }

    private void resize() {
        int currentLength = elements.length;
        int newLength = currentLength + currentLength / 2;
        T[] newElements = (T[]) new Object[newLength];

        // копируем в новый массив
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // переключим ссылку на новый массив
        this.elements = newElements;
    }

    private boolean isFool() {
        return count == elements.length;
    }

    @Override
    public boolean contain(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }

    @Override
    public void remove(T element) {
        int deleteElement = 0;
        int index;
        for (int i = 0; i < count; i++) {
            if (element.equals(elements[i])) {
                System.out.println("Элемент " + elements[i] + " найден");
                deleteElement = i;
                removeAt(deleteElement);
                break;
            }
        }
    }
    @Override
    public void removeAt(int index) {
        if (index >= 0 && index < count) {
            for (int i = index; i < count - 1; i++) {
                elements[i] = elements[i + 1];
            }
            elements[count] = null;
            count--;
            for (int i = 0; i < count; i++) {
                System.out.print(elements[i] + " ");
            }
        }
    }
}
