import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] array = {13, 43, 23, 88, 5, 7};

        // 1. Считать размер массива

        System.out.println("In array " + array.length + " elements!");

        // 2. Считать элементы массива

        System.out.println(Arrays.toString(array));

        // 3. Вывести количетсво локальных минимумов для введенного числа

        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (i==0) {
                if (array[i] < array[i + 1]) {
                    count++;
                }
            } else if (i == array.length-1) {
                if (array[i] < array[i-1]) {
                    count++;
                }
            } else {
                if (array[i] < array[i-1] && array[i] < array[i+1]) {
                    count++;
                }
            }
        }
        System.out.println("Найдено локальных минимумов: " + count);
    }
}