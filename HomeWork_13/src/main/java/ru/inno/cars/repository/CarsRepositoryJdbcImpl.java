package ru.inno.cars.repository;

import ru.inno.cars.models.Car;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CarsRepositoryJdbcImpl implements CarsRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from auto order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into auto(model, color, number)" + "values (?, ?, ?, ?)";

    private final DataSource dataSource;

    public CarsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Car> carRowMapper = row -> {
        try {
            return Car.builder()
                    .id(row.getLong("id"))
                    .model(row.getString("model"))
                    .color(row.getString("color"))
                    .number(row.getString("number"))
                    .driver_id(row.getInt("driver_id"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car auto = carRowMapper.apply(resultSet);
                    cars.add(auto);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return cars;
    }

    @Override
    public void save(Car auto) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, auto.getModel());
            preparedStatement.setString(2, auto.getColor());
            preparedStatement.setString(3, auto.getNumber());
            preparedStatement.setInt(4, auto.getDriver_id());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert auto");
            }

            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    auto.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't obtain generated id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
