package ru.inno.cars.models;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Car {
    private Long id;
    private String model;
    private String color;
    private String number;
    private Integer driver_id;
}