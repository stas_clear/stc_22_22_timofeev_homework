package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;
import ru.inno.cars.repository.CarsRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"-action"})
    private String action;

    public static void main(String[] args) {
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();

        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        Scanner scanner = new Scanner(System.in);
        Main main = new Main();

        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);

       if (main.action.equals("read")) {
            List<Car> carsFromDb = carsRepository.findAll();
            for (Car car : carsFromDb) {
                System.out.println(car);
            }
        } else if (main.action.equals("write")) {
            while (true) {
                System.out.println("Insert information about new car:");
                System.out.print("Model: ");
                String model = scanner.nextLine();

                System.out.print("Color: ");
                String color = scanner.nextLine();

                System.out.print("Number: ");
                String number = scanner.nextLine();

                System.out.print("Driver Id: ");
                Integer driver_id = scanner.nextInt();

                scanner.nextLine();

                Car car = Car.builder()
                        .model(model)
                        .color(color)
                        .number(number)
                        .driver_id(driver_id)
                        .build();

                carsRepository.save(car);

                System.out.print("Exit? (Yes or No): ");
                String exit = scanner.nextLine();
                if (exit.equals("Yes")) {
                    break;
                }
            }
       } else {
            System.err.println("Incorrect value of action");
       }
    }
}