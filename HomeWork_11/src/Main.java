public class Main {
    public static void main(String[] args) {
        CarRepository carRepository = new CarRepositoryFileBasedImpl("car.txt");
        Car jigul = new Car("a357oe", "Lada", "Red", 12345.6, 50000);
        carRepository.save(jigul);
        System.out.println(carRepository.findAll() + "\n");
        System.out.println(carRepository.findNumberByColorOrMileage("Black", 0) + "\n");
        System.out.println(carRepository.findAllUniqueCarInRangeOfPrice(70000, 85000) + "\n");
        System.out.println(carRepository.findColorByMinimalPrice() + "\n");
        System.out.println(carRepository.findAveragePriceOfCar("Camry") + "\n");
    }
}
