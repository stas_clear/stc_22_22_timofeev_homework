public class Main {
    public static void main(String[] args) {
        Rectangle rect = new Rectangle(0, 0, 4, 8);
        Square square = new Square(0, 0, 4);
        Ellipse ellipse = new Ellipse(0, 0, 3, 6);
        Circle circle = new Circle(0, 0, 7);

        System.out.println("Прямоугольник S = " + rect.calcArea() + ", P = " + rect.calcPerimeter());
        rect.move(26, 38);

        System.out.println("Квадрат S = " + square.calcArea() + ", P = " + square.calcPerimeter());
        rect.move(76, 52);

        System.out.println("Эллипс S = " + ellipse.calcArea() + ", P = " + ellipse.calcPerimeter());
        rect.move(13, 41);

        System.out.println("Площадь круга: " + circle.calcArea());
        circle.move(16, 78);


    }
}