public abstract class Figure {
    private int x;
    private int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(int toX, int toY) {
        this.x = toX;
        this.y = toY;
        System.out.println("Новые координаты: " + getX() + ", " + getY());
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    abstract double calcArea();
    abstract double calcPerimeter();
}