public class Rectangle extends Figure {
    private final int a;
    private final int b;

    public Rectangle(int x, int y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public double calcArea() {
        return a * b;
    }
    public double calcPerimeter() {
        return 2 * (a + b);
    }

}