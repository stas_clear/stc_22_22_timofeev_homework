public class Ellipse extends Figure {
    private final double r1;
    private final double r2;

    public Ellipse(int x, int y, double r1, double r2) {
        super(x, y);
        this.r1 = r1;
        this.r2 = r2;
    }
    public double calcArea() {
        return Math.PI * r1 * r2;
    }
    public double calcPerimeter() {
        return 2 * Math.PI * Math.sqrt((r2 * r2 + r1 * r1) / (2 * 1.0));
    }

}